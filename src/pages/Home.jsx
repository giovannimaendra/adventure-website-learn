import '../App.css'
import Cards from '../component/Cards'
import HeroSection from '../component/HeroSection'

const Home = () => {
  return (
    <>
      <HeroSection />
      <Cards />
    </>
  )
}

export default Home

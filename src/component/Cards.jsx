import CardItem from "./CardItem"
import '../assets/css/cards.css'
import pic9 from '../assets/images/img-9.jpg'
import pic2 from '../assets/images/img-2.jpg'

const Cards = () => {
  return (
    <div className='cards'>
      <h1>Check out these EPIC Destinations</h1>
      <div className="cards__container">
        <div className="cards__wrapper">
          <ul className="cards__items">
            <CardItem src={pic9}
              text='Explore the hiddenn waterfall deep inside the Amazon Jungle'
              label='Adventure'
              path='/services'
            />
            <CardItem src={pic2}
              text='Travel through the Islands of Bali in a Private Cruise'
              label='Luxury'
              path='/services'
            />
          </ul>
          <ul className="cards__items">
            <CardItem src={pic9}
              text='Explore the hiddenn waterfall deep inside the Amazon Jungle'
              label='Adventure'
              path='/services'
            />
            <CardItem src={pic2}
              text='Travel through the Islands of Bali in a Private Cruise'
              label='Luxury'
              path='/services'
            />
            <CardItem src={pic2}
              text='Travel through the Islands of Bali in a Private Cruise'
              label='Luxury'
              path='/services'
            />
          </ul>
        </div>
      </div>
    </div>
  )
}

export default Cards

import './App.css';
import Navbar from './component/Navbar';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import Home from './pages/Home';
import SignUp from './pages/SignUp';
import Services from './pages/Services';
import Products from './pages/Products';
import Footer from './component/Footer';

function App() {
  return (
    <>
      <Router>
        <Navbar />
        <Switch>
          <Route path='/' exact component={Home} />
          <Route path='/sign-up' exact component={SignUp} />
          <Route path='/services' exact component={Services} />
          <Route path='/products' exact component={Products} />
        </Switch>
        <Footer />
      </Router>
    </>
  );
}

export default App;
